;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: CL-CARBON -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          utils.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       Utility macros and functions
;;;; Programmer:    David Steuber
;;;; Date Started:  1/21/2005
;;;;
;;;; $Id: utils.lisp,v 1.1.1.1 2005/04/29 20:19:03 dsteuber Exp $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-carbon)

(defun cf-string-make-constant-string (s)
  (#___CFStringMakeConstantString s))

(defmacro const-cfstring (str)
  (let ((s (gensym)))
    `(ccl::with-cstr (,s ,str) (cl-carbon::cf-string-make-constant-string ,s))))

(defun make-cfstring (str)
  "Allocates a CFString object stored in a MACPTR which must be
  CFRelease(d) when no longer needed."
  (ccl::with-cstr (cstr str)
     (#_CFStringCreateWithCString (ccl:%null-ptr) cstr #$kCFStringEncodingMacRoman)))

(defun cf-release (cf-ptr)
  (#_CFRelease cf-ptr))

(defmacro with-cfstring ((sym str) &rest body)
  "Create, use, and then release a CFString."
  `(let ((,sym (make-cfstring ,str)))
     (unwind-protect (progn ,@body)
       (cl-carbon::cf-release ,sym))))
  
(defmacro with-cfstrings (speclist &body body)
  "Create, use, and then release CFStrings."
  (ccl::with-specs-aux 'with-cfstring speclist body))

(defmacro require-noerror (&body forms)
  (let* ((err (gensym))
         (body (reverse `(let (,err)))))
    (dolist (form forms (nreverse body))
      (push `(setf ,err ,form) body)
      (push `(assert (eql ,err #.#.(read-from-string "#$noErr"))) body))))

(defmacro case-equal (exp &body clauses)
  (let ((temp (gensym)))
    `(let ((,temp ,exp))
       (cond ,@(mapcar #'(lambda (clause)
                           (destructuring-bind (keys . clause-forms) clause
                             (if (eq keys 'otherwise)
                                 `(t ,@clause-forms)
                                 (if (atom keys)
                                     `((equal ,temp ,keys) ,@clause-forms)
                                     `((member ,temp ',keys :test #'equal)
                                       ,@clause-forms)))))
                       clauses)))))

(defun show-alert (s)
  (ccl::with-pstr (message-str s)
    (#_StandardAlert #$kAlertNoteAlert message-str (%null-ptr) (%null-ptr) (%null-ptr))))

(defun make-lisp-string-from-cfstringref (ptr &optional (encoding #$kCFStringEncodingMacRoman))
  "Use the CFStringRef in ptr to make a Lisp string useing the provided encoding."
  (rlet ((cstr (:* :char) (#_CFStringGetCStringPtr ptr encoding)))
    (if (ccl::%null-ptr-p cstr)
        (rlet ((buffer (:array :char 1024)))
          (when (= 1 (#_CFStringGetCString ptr buffer 1024 encoding))
            (ccl::%get-cstring buffer)))
        (ccl::%get-cstring (ccl::%get-ptr cstr)))))
