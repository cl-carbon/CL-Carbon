;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: cl-user -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          package.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       package definitions for CL-Carbon
;;;; Programmer:    David Steuber
;;;; Date Started:  1/21/2005
;;;;
;;;; $Id: package.lisp,v 1.1.1.1 2005/04/29 20:19:03 dsteuber Exp $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-user)

;;; --------------------------------------------------
;;; Package: CL-CARBON
;;; --------------------------------------------------

(eval-when (:COMPILE-TOPLEVEL :LOAD-TOPLEVEL :EXECUTE)
  (defpackage :cl-carbon
    (:documentation
     "The package for CL-Carbon")
    (:nicknames :carbon)
    (:use common-lisp ccl)
    (:import-from :ccl)
    (:export "APPLICATION"
             "APPLICATION-WINDOWS"
             "RUN-APPLICATION"
             "CLEANUP-APPLICATION"
             "WINDOW"
             "WINDOW-OWNER"
             "WINDOW-PTR"
             "CREATE-WINDOW"
             "SHOW-WINDOW"
             "HIDE-WINDOW"
             "WINDOW-CLOSING"
             "WINDOW-CLOSE"
             "GET-EVENT-TYPE-SPECS"
             "HANDLE-EVENT"
             "MENU-COMMAND"
             "EVENT-TYPE-SPEC" 
             "MAKE-EVENT-TYPE-SPEC"
             "INSTALL-EVENT-HANDLER"
             "REMOVE-EVENT-HANDLER"
             "ADD-EVENT-TYPES-TO-HANDLER"
             "REMOVE-EVENT-TYPES-FROM-HANDLER"
             "ENABLE-DEBUG-LOG"
             "DISABLE-DEBUG-LOG"
             "ENABLE-DEBUGGING"
             "DISABLE-DEBUGGING"
             "DEBUG-LOG"
             "INT32-TO-STRING"
             "WITH-CFSTRING"
             "WITH-CFSTRINGS"
             "CONST-CFSTRING"
             "MAKE-CFSTRING"
             "REQUIRE-NOERROR"
             "SHOW-ALERT"
             "MAKE-LISP-STRING-FROM-CFSTRINGREF"
             "NIB"
             "NIB-FILE-NAME"
             "NIB-RESOURCE-NAME")))
