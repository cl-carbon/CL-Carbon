;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: CL-CARBON -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          application.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       Define the carbon::application class
;;;; Programmer:    David Steuber
;;;; Date Started:  1/26/2005
;;;;
;;;; $Id: application.lisp,v 1.2 2005/05/04 09:15:54 dsteuber Exp $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-carbon)

(defclass application (event-target)
  ((windows :reader application-windows :initarg :windows :initform nil
            :documentation "The window objects belonging to this application"))
  (:documentation "The Carbon application"))

(defgeneric run-application (application)
  (:documentation
   "Runs the Carbon event loop."))

(defgeneric cleanup-application (application)
  (:documentation
   "Performs required cleanup actions after exiting event loop."))

(defmethod initialize-instance :after ((app application) &rest initargs)
  (declare (ignore initargs))
  (enable-debug-log (make-pathname :directory (ccl::getenv "HOME") :name "CL-Carbon application debug" :type "log"))
  (debug-log "Initializing Carbon application -- CARBON:APPLICATION.INITIALIZE-APPLICATION called~%")
  (require-noerror
    (install-event-handler app
                           (#_GetApplicationEventTarget)
                           (get-event-type-specs app))))

(defmethod get-event-type-specs ((app application))
  `(,(carbon:make-event-type-spec #$kEventClassCommand #$kEventCommandProcess)))

(defmethod run-application ((app application))
  (#_RunApplicationEventLoop))

(defmethod run-application :after ((app application))
  (cleanup-application app)
  (quit))

(defmethod cleanup-application ((app application))
  (disable-debug-log))
