;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: asdf -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          cl-carbon.asd
;;;; Project:       CL-Carbon
;;;; Purpose:       System definition file for CL-Carbon library
;;;; Programmer:    David Steuber
;;;; Date Started:  1/21/2005
;;;;
;;;; $Id: $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(require :asdf)

(in-package #:asdf)

;;; CL-Carbon system definition
(asdf:defsystem #:cl-carbon
    :description "CL-Carbon: A wrapper for Apple's Carbon API"
    :version "0.1"
    :author "David Steuber"
    :depends-on ()
    :components ((:file "package")
                 (:file "debug" :depends-on ("package"))
                 (:file "framework" :depends-on ("debug"))
                 (:file "utils" :depends-on ("framework"))
                 (:file "event" :depends-on ("utils"))
                 (:file "nib" :depends-on ("utils"))
                 (:file "application" :depends-on ("event"))
                 (:file "window" :depends-on ("application" "nib"))))
    