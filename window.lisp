;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: CL-CARBON -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          window.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       Carbon window proxies
;;;; Programmer:    David Steuber
;;;; Date Started:  1/29/2005
;;;;
;;;; $Id: window.lisp,v 1.1.1.1 2005/04/29 20:19:03 dsteuber Exp $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-carbon)

(defclass window (event-target)
  ((window-ptr :accessor window-ptr :initform nil
               :documentation "The MACPTR that holds the Carbon WindowRef"))
  (:documentation
   "Proxy for a Carbon window"))

(defmethod initialize-instance :around ((w window) &rest initargs)
  (let ((m "CARBON:WINDOW.INITIALIZE-INSTANCE :AROUND") v)
    (debug-log "~A -- called with args ~S~%" m initargs)
    (when (next-method-p)
      (setf v (apply #'call-next-method (push w initargs))))
    (debug-log "~A -- owner set to ~S~%" m (slot-value w 'owner))
    (create-window w)
    ;;
    ;; TODO
    ;;
    ;; Need code here to deal with the possibility of create-window signaling a condition
    ;; so that the window-ptr can be disposed of properly, eg tell OS X to destroy the
    ;; window.
    ;;
    (with-slots (window-ptr) w
      (install-event-handler w (#_GetWindowEventTarget window-ptr)
                             (get-event-type-specs w)))
    v))

(defgeneric create-window (window)
  (:documentation "Create the actual window"))

(defgeneric show-window (window))
(defgeneric hide-window (window))

(defmethod show-window ((w window))
  (with-slots (window-ptr) w
    (when window-ptr (#_ShowWindow window-ptr))))

(defmethod hide-window ((w window))
  (with-slots (window-ptr) w
    (when window-ptr (#_HideWindow window-ptr))))

(defgeneric window-closing (window)
  (:documentation "Time to cleanup.  The real window is closing."))

(defmethod window-closing ((w window))
  (remove-event-handler w))

(defgeneric window-close (window)
  (:documentation "Return true if allowed to close, else return false."))

(defmethod window-close ((w window))
  t)

(defgeneric handle-window-close (window))
(defmethod handle-window-close ((w window))
  (let ((can-close (window-close w)))
    (if can-close
        (progn
          (window-closing w)
          nil)
        t)))

(defmethod handle-event ((w window)
                         (class (eql #$kEventClassWindow))
                         (kind (eql #$kEventWindowClose))
                         next-handler event user-data)
  (declare (ignore next-handler event user-data))
  (handle-window-close w))
