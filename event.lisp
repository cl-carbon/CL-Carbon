;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: CL-CARBON -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          event.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       Carbon Event Manager abstraction
;;;; Programmer:    David Steuber
;;;; Date Started:  1/27/2005
;;;;
;;;; $Id: event.lisp,v 1.2 2005/05/04 09:15:54 dsteuber Exp $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-carbon)

(defclass event-target ()
  ((event-handler-callback :initform (ccl::%null-ptr))
   (event-handler-ref :initform (ccl::%null-ptr)))
  (:documentation "An object that receives Carbon events"))

(defstruct (event-type-spec
             (:constructor make-event-type-spec (event-class event-kind))
             (:conc-name ets-))
  (event-class 0 :type (unsigned-byte 32))
  (event-kind  0 :type (unsigned-byte 32)))

(defgeneric get-event-type-specs (event-target)
  (:documentation "Returns a list of event-type-spec objects for installing the event handler."))

(defgeneric handle-event (target class kind next-handler event user-data)
  (:documentation
   "Handle Carbon events.  Return T when the event is handled or NIL otherwise."))

(defmethod handle-event ((target event-target) class kind next-handler event user-data)
  (declare (ignore class kind next-handler event user-data))
  nil)

(defgeneric menu-command (event-target command)
  (:documentation
   "Handle menu commands.  Return T when command is handled or NIL otherwise."))

(defmethod  menu-command ((et event-target) command)
  (declare (ignore command))
  nil)

(defgeneric install-event-handler (event-target target event-type-specs)
  (:documentation "Installs an event handler"))

(defmethod install-event-handler ((et event-target) target event-type-specs)
  (let* ((num-specs (length event-type-specs))
         (offset 0)
         (event-specs (ccl::malloc (* num-specs (ccl::record-length :<e>vent<t>ype<s>pec)))))
    (dolist (ets event-type-specs)
      (setf (ccl::%get-unsigned-long event-specs offset) (ets-event-class ets))
      (incf offset (ccl::record-length :unsigned))
      (setf (ccl::%get-unsigned-long event-specs offset) (ets-event-kind ets))
      (incf offset (ccl::record-length :unsigned)))
    (rlet ((ehr :<e>vent<h>andler<r>ef))
        (with-slots (event-handler-callback event-handler-ref) et
          (let ((retval (#_InstallEventHandler target
                                               (#_NewEventHandlerUPP (setf event-handler-callback
                                                                           (make-event-target-callback et)))
                                               num-specs
                                               event-specs
                                               (ccl::%null-ptr)
                                               ehr)))
            (ccl::free event-specs)
            (setf event-handler-ref (ccl::%get-ptr ehr))
            (debug-log "Installed event handler: ~S~%" event-handler-ref)
            retval)))))

(defgeneric remove-event-handler (event-target)
  (:documentation "Removes (uninstalls) an event handler"))

(defmethod remove-event-handler ((et event-target))
  (with-slots (event-handler-callback event-handler-ref) et
    (debug-log "Removing event handler: ~S~%" event-handler-ref)
    (#_RemoveEventHandler event-handler-ref)
    (delete-event-target-callback event-handler-callback)))

(defgeneric add-event-types-to-handler (event-target event-specs))

(defgeneric remove-event-types-from-handler (event-target event-specs))

(defmethod add-event-types-to-handler ((et event-target) event-specs)
  (frob-event-types-for-handler (lambda (ehr count typespecs)
                                  (#_AddEventTypesToHandler ehr count typespecs))
                                (slot-value et 'event-handler-ref)
                                event-specs))

(defmethod remove-event-types-from-handler ((et event-target) event-specs)
  (frob-event-types-for-handler (lambda (ehr count typespecs)
                                  (#_RemoveEventTypesFromHandler ehr count typespecs))
                                (slot-value et 'event-handler-ref)
                                event-specs))

(defun frob-event-types-for-handler (frob-fn event-handler-ref event-type-specs)
  (let* ((num-specs (length event-type-specs))
         (offset 0)
         (event-specs (ccl::malloc (* num-specs (ccl::record-length :<e>vent<t>ype<s>pec)))))
    (dolist (ets event-type-specs)
      (setf (ccl::%get-unsigned-long event-specs offset) (ets-event-class ets))
      (incf offset (ccl::record-length :unsigned))
      (setf (ccl::%get-unsigned-long event-specs offset) (ets-event-kind ets))
      (incf offset (ccl::record-length :unsigned)))
    (let ((retval (funcall frob-fn event-handler-ref num-specs event-specs)))
      (ccl::free event-specs)
      retval)))

(defmethod handle-event ((et event-target)
                         (class (eql #$kEventClassCommand))
                         (kind  (eql #$kEventCommandProcess))
                         next-handler event user-data)
  (declare (ignore next-handler user-data))
  (rlet ((command :<hic>ommand))
    (#_GetEventParameter event #$kEventParamDirectObject #$typeHICommand
                         (ccl::%null-ptr) (ccl::record-length :<hic>ommand)
                         (ccl::%null-ptr) command)
    (menu-command et (ccl::pref command :<hic>ommand.command<id>))))

(defun make-event-target-callback (et)
  (let (fn-carbon-event-handler)
    (declare (special fn-carbon-event-handler))
    (ccl:defcallback fn-carbon-event-handler
        (:<e>vent<h>andler<c>all<r>ef next-handler :<e>vent<r>ef event (:* t) user-data :<oss>tatus)
      (let ((class (#_GetEventClass event))
            (kind  (#_GetEventKind  event)))
        (declare (dynamic-extent class kind))
        (debug-log "Callback CARBON-EVENT-HANDLER: event-handler-ref = ~S; Class: '~A' Kind: ~A~%"
                   (slot-value et 'event-handler-ref) (int32-to-string class) kind)
        (multiple-value-bind (r c)
            (ignore-errors
              (handle-event et class kind next-handler event user-data))
          (declare (dynamic-extent r c))
          (when c
            (debug-log "Condition signaled from CARBON-EVENT-HANDLER: < ~A >~%" c))
          (if r #$noErr #$eventNotHandledErr))))
    fn-carbon-event-handler))

;; this function is based on code that Gary Byers posted to openmcl-devel
(defun delete-event-target-callback (pointer)
  (with-lock-grabbed (ccl::*callback-lock*)
    (let ((index (dotimes (i (length ccl::%pascal-functions%))
                   (when (eql (ccl::pfe.routine-descriptor (svref ccl::%pascal-functions% i))
                              pointer)
                     (return i)))))
      (when index
        (let ((entry (svref ccl::%pascal-functions% index)))
          (setf (svref ccl::%pascal-functions% index) nil)
          (ccl::free (ccl::pfe.routine-descriptor entry))
          t)))))

