;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: CL-CARBON -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          debug.lisp
;;;; Project:       CL-Carbon
;;;; Purpose:       Provide debugging assistance for CL-Carbon based apps
;;;; Programmer:    David Steuber
;;;; Date Started:  1/21/2005
;;;;
;;;; $Id: $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-carbon)

(defparameter *debug-log-file* nil)

(defun enable-debug-log (log-file-spec)
  (when (member :debug *features*)
    (when *debug-log-file*
      (close *debug-log-file*))
    (setf *debug-log-file*
          (open log-file-spec :direction :output
                :if-exists :supersede))))

(defun disable-debug-log ()
  (when *debug-log-file*
    (close *debug-log-file*))
  (setf *debug-log-file* nil))

(defun enable-debugging ()
  (pushnew :debug *features*))

(defun disable-debugging ()
  (setf *features* (delete :debug *features*)))

(defun write-debug-log (cs &rest args)
  (when *debug-log-file*
    (let ((fargs (cons *debug-log-file* (cons cs args))))
      (apply #'format fargs)
      (force-output *debug-log-file*))))

(defun int32-to-string (n)
  (with-output-to-string (s)
    (dolist (i '(24 16 8 0))
      (princ (code-char (ldb (byte 8 i) n)) s))))

(defmacro debug-log (format-control-string &rest format-args)
  (if (member :debug *features*)
      `(carbon::write-debug-log ,format-control-string ,@format-args)
      (values)))

(enable-debugging)
